
### Run on Vagrant


    vagrant plugin install vagrant-hostmanager
    
    # Add to /etc/hosts
    192.168.50.10 k8s0
    192.168.50.11 k8s1
    192.168.50.12 k8s2

    # Install kubectl
    curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
    
    vagrant up

# If vagrant ssh doesn't work

If "WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!",
remove entries from your known_hosts.

ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R 192.168.50.10
