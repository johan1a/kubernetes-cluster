# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|

  config.vm.box_check_update = false

  config.hostmanager.enabled = true
  config.hostmanager.manage_guest = true


  N = 2
  (0..N).each do |machine_index|
    hostname = "k8s#{machine_index}"

    config.vm.define hostname do |machine|

      machine.vm.box = "centos/7"

      machine.vm.hostname = hostname
      node_ip = "192.168.50.#{10 + machine_index}"

      machine.vm.network "private_network", ip: node_ip
      machine.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "~/.ssh/me.pub"
      machine.vm.provision "shell", inline: "cat ~vagrant/.ssh/me.pub >> ~vagrant/.ssh/authorized_keys"
      machine.vm.provision "shell", inline: "echo 'vagrant:vagrant' | chpasswd"

      machine.vm.provider "virtualbox" do |v|
        v.name = hostname
        v.memory = 2048
        v.gui = false
      end

      if machine_index == 2
        machine.vm.provision :ansible do |ansible|
          ansible.limit = "all"
          ansible.verbose = "v"
          ansible.playbook = "config-cluster.yaml"
          ansible.inventory_path = "./inventory/test/hosts.ini"
          ansible.become = true
          ansible.extra_vars = {
            reset_cluster: true,
            reset_nodes: true,
            user_name: "vagrant",
            ansible_sudo_pass: "vagrant",
            ansible_ssh_user: "vagrant",
            ansible_ssh_private_key_file: '~/.ssh/id_rsa'
          }
        end
      end
    end
  end
end
