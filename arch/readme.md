
# Troubleshooting
##  If there is some cgroup problem (check journald -feu kubelet)

## check
```
docker info | grep -i cgroup

#Output
Cgroup Driver: systemd
Cgroup Version: 2
  cgroupns
```

```
update /var/lib/kubelet/config.yaml
to:
cgroupDriver: systemd
```


# To tear down the cluster:
```
sudo kubeadm reset
```
