
sudo pacman -S base-devel docker ethtool ebtables socat conntrack-tools --needed --noconfirm
# Requires yay
paru -S kubernetes-bin --noconfirm --needed

sudo swapoff -a
echo 1 | sudo tee /proc/sys/net/ipv4/ip_forward
sudo modprobe br_netfilter

sudo systemctl enable docker
sudo systemctl enable kubelet.service
