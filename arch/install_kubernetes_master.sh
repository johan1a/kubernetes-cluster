#!/bin/sh


# sudo systemctl daemon-reload
# sudo systemctl restart docker.service
# systemctl status docker.service
# sudo systemctl restart kubelet.service
# systemctl status kubelet.service

echo ""
echo ""
echo ""
echo Running kubeadm init. If it hangs at Waiting for the kubelet to boot up, run from another terminal: sudo systemctl restart kubelet
echo ""
echo ""
echo ""

# if your kernel is too new: --ignore-preflight-errors=SystemVerification
sudo kubeadm init

# in another terminal:
# sudo systemctl restart kubelet

rm -rf $HOME/.kube
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config


export kube_network_url=https://cloud.weave.works/k8s/net\?k8s-version=$(kubectl version | base64 | tr -d "\n")
kubectl apply -f $kube_network_url

kubectl taint nodes --all node-role.kubernetes.io/master-

# Dirty dirty hack
# https://github.com/kubernetes/kubeadm/issues/998
# kubectl -n kube-system get deployment coredns -o yaml | \
# probably not needed due to allow privileges true in kubelet args
#   sed 's/allowPrivilegeEscalation: false/allowPrivilegeEscalation: true/g' | \
#   kubectl apply -f -
kubectl label nodes $(hostname) master=true
#kubectl label nodes urdatorn2 master=true

#kubectl apply -f ../roles/master/files/default-ingress.yml
#kubectl apply -f ../roles/master/files/nginx-ingress-controller.yml


#sen deploy med deploy repot

