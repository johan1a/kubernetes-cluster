#!/bin/bash

ansible-playbook -b config-cluster.yaml \
  -i inventory/test/hosts.ini \
  -u vagrant -v \
  -e reset_cluster=True \
  -e reset_nodes=True \
  -e local_user=$USER \
  -e ansible_sudo_pass=vagrant

